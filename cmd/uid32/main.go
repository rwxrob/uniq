package main

import (
	"fmt"

	"gitlab.com/rwxrob/uniq"
)

func main() {
	fmt.Print(uniq.Base32())
}
